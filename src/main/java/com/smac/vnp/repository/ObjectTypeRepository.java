package com.smac.vnp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smac.vnp.entity.ObjectType;


@Repository
public interface ObjectTypeRepository extends JpaRepository<ObjectType, Long>{
	 
}
