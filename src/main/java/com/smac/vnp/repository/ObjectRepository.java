package com.smac.vnp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.smac.vnp.entity.Object;

public interface ObjectRepository extends JpaRepository<Object, Long>{

}
