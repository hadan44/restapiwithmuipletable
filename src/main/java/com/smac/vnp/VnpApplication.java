package com.smac.vnp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VnpApplication {

	public static void main(String[] args) {
		SpringApplication.run(VnpApplication.class, args);
	}

}
