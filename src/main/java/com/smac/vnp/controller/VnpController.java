package com.smac.vnp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smac.vnp.entity.ObjectType;
import com.smac.vnp.entity.Object;
import com.smac.vnp.service.ObjectService;
import com.smac.vnp.service.ObjectTypeService;

@RestController
public class VnpController {
	 
		@Autowired
		private ObjectTypeService objectTypeService;
		 @Autowired
		 private ObjectService objectService;
		
		@RequestMapping(value="/type", method=RequestMethod.GET)
		public List<ObjectType> getAll(){
			 return (List<ObjectType>) objectTypeService.getAll();
		}
		
		@RequestMapping(value="/object", method=RequestMethod.GET)
		public List<Object> getAllObject(){
			 return (List<Object>) objectService.getAllObject();
		}
}
