package com.smac.vnp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "function")
public class Function {
	
	@Id
	@GeneratedValue
	@Column(name = "FUNC_ID", updatable = false, nullable = false)
	private long func_id;
	
	@Column(name = "OBJECT_ID")
	private String obj_id;
	
	@Column(name = "FUNC_NAME")
	private String func_name;
	
	@ManyToOne
	@JsonBackReference
    @JoinColumn(name="OBJECT_ID", referencedColumnName="OBJECT_ID", updatable = false, insertable = false, nullable = false)
    private Object object;

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public long getFunc_id() {
		return func_id;
	}

	public void setFunc_id(long func_id) {
		this.func_id = func_id;
	}

	public String getObj_id() {
		return obj_id;
	}

	public void setObj_id(String obj_id) {
		this.obj_id = obj_id;
	}

	public String getFunc_name() {
		return func_name;
	}

	public void setFunc_name(String func_name) {
		this.func_name = func_name;
	}

	public Function(long func_id, String obj_id, String func_name) {
		super();
		this.func_id = func_id;
		this.obj_id = obj_id;
		this.func_name = func_name;
	}
	
	public Function() {}
}
