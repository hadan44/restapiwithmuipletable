package com.smac.vnp.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "object")
public class Object {
	
	@Id
	@GeneratedValue
	@Column(name = "OBJECT_ID", updatable = false, nullable = false)
	private long obj_id;
	
	@Column(name = "OBJECT_TITLE")
	private String obj_title;
	
	@Column(name = "OBJECT_IMAGEURL")
	private String obj_image_url;
	
	@Column(name = "PARENTOBJECT_ID")
	private int parent_obj_id;
	
	@ManyToOne
	@JsonBackReference
    @JoinColumn(name="PARENTOBJECT_ID", referencedColumnName="OBJECTTYPE_ID", updatable = false, insertable = false, nullable = false)
    private ObjectType objectType;
	
	@JsonManagedReference
	@OneToMany(targetEntity = Function.class,cascade = CascadeType.ALL,mappedBy="object",fetch = FetchType.EAGER) //or FetchType.LAZY
	private Set<Function> func;
	
	public Set<Function> getFunc() {
		return func;
	}

	public void setFunc(Set<Function> func) {
		this.func = func;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public long getObj_id() {
		return obj_id;
	}

	public void setObj_id(long obj_id) {
		this.obj_id = obj_id;
	}

	public String getObj_title() {
		return obj_title;
	}

	public void setObj_title(String obj_title) {
		this.obj_title = obj_title;
	}

	public String getObj_image_url() {
		return obj_image_url;
	}

	public void setObj_image_url(String obj_image_url) {
		this.obj_image_url = obj_image_url;
	}

	public int getParent_obj_id() {
		return parent_obj_id;
	}

	public void setParent_obj_id(int parent_obj_id) {
		this.parent_obj_id = parent_obj_id;
	}

	public Object(long obj_id, String obj_title, String obj_image_url, int parent_obj_id) {
		super();
		this.obj_id = obj_id;
		this.obj_title = obj_title;
		this.obj_image_url = obj_image_url;
		this.parent_obj_id = parent_obj_id;
	}
	
	public Object() {}
}
