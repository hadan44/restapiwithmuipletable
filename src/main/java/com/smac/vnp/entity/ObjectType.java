package com.smac.vnp.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "OBJECTTYPE")
public class ObjectType {
	@Id
	@Column(name = "OBJECTTYPE_ID", updatable = false, nullable = false)
	private long objType_id;
	
	@Column(name = "TYPE_ID")
	private String type_id;
	
	@Column(name = "OBJECTTYPE_NAME")
	private String objType_name;
	
	@Column(name = "OBJECTTYPE_TITLE")
	private String objType_title;

	
	
	@JsonManagedReference
	@JsonBackReference
	@OneToMany(targetEntity = Object.class,cascade = CascadeType.ALL,mappedBy="objectType",fetch = FetchType.LAZY) //or FetchType.EAGER
	private Set<Object> object;
	
	public long getObjType_id() {
		return objType_id;
	}

	public void setObjType_id(long objType_id) {
		this.objType_id = objType_id;
	}


	public Set<Object> getObject() {
		return object;
	}

	public void setObject(Set<Object> object) {
		this.object = object;
	}

	public String getType_id() {
		return type_id;
	}

	public void setType_id(String type_id) {
		this.type_id = type_id;
	}

	public String getObjType_name() {
		return objType_name;
	}

	public void setObjType_name(String objType_name) {
		this.objType_name = objType_name;
	}

	public String getObjType_title() {
		return objType_title;
	}

	public void setObjType_title(String objType_title) {
		this.objType_title = objType_title;
	}

	public ObjectType(long objType_id, String type_id, String objType_name, String objType_title) {
		super();
		this.objType_id = objType_id;
		this.type_id = type_id;
		this.objType_name = objType_name;
		this.objType_title = objType_title;
	}

	public ObjectType() {}
	

}
