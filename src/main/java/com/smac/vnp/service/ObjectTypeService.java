package com.smac.vnp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.smac.vnp.entity.ObjectType;

@Service
public interface ObjectTypeService {
	List<ObjectType> getAll();
}
