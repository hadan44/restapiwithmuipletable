package com.smac.vnp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smac.vnp.entity.ObjectType;
import com.smac.vnp.repository.ObjectTypeRepository;

@Service
public class ObjectTypeServiceImp implements ObjectTypeService{

	@Autowired
	private ObjectTypeRepository repo;
	
	@Override
	public List<ObjectType> getAll() {
		return(List<ObjectType>) repo.findAll();
	}

}
