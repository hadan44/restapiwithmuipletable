package com.smac.vnp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smac.vnp.entity.Object;
import com.smac.vnp.repository.ObjectRepository;

@Service
public class ObjectServiceImp implements ObjectService{

	@Autowired
	private ObjectRepository obrepo;

	@Override
	public List<Object> getAllObject() {
		return (List<Object>) obrepo.findAll();
	}
}
	
	
