package com.smac.vnp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.smac.vnp.entity.Object;

@Service
public interface ObjectService {
	List<Object> getAllObject();
}
